package com.gsu.mk.easyhire.data.dto

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class FBObject (

    @Expose
    @SerializedName("audio")
    val audio: String?,

    @Expose
    @SerializedName("description")
    val description: String,

    @Expose
    @SerializedName("image")
    val image: String?,

    @Expose
    @SerializedName("latitude")
    val latitude: Double,

    @Expose
    @SerializedName("longitude")
    val longitude: Double,

    @Expose
    @SerializedName("text")
    val text: String,

    @Expose
    @SerializedName("title")
    val title: String
) : Parcelable