package com.gsu.mk.easyhire.presentation.feature.ar.fragmentview

import android.Manifest
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.AttributeSet
import android.util.Log
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.ar.core.*
import com.google.ar.sceneform.ArSceneView
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Scene
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.presentation.feature.ar.fragmentview.IARObjectSceneFragmentView.Listener
import com.gsu.mk.easyhire.presentation.feature.ar.node.ARNode
import com.gsu.mk.easyhire.presentation.feature.util.bind
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener

class ARObjectSceneFragmentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), IARObjectSceneFragmentView,
    Scene.OnUpdateListener {

    private val arView: ArSceneView by bind(R.id.object_ar_scene_view)
    private var session: Session? = null
    private var shoodConfigurateSession = false

    override fun populate(activity: Activity) {
        Dexter.withActivity(activity)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object: PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    setupSession()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {

                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    Toast.makeText(context, "Необходим доступ к камере", Toast.LENGTH_SHORT).show()
                }
            }).check()
        initScreenView()
    }

    private fun initScreenView() {
        arView.scene.addOnUpdateListener(this)
    }

    override fun onUpdate(frameTime: FrameTime?) {
        val frame = arView.arFrame
        val updateAugmentedImage: Collection<AugmentedImage> = frame.getUpdatedTrackables(AugmentedImage::class.java)
        updateAugmentedImage.forEach { image ->
            if (image.trackingState == TrackingState.TRACKING) {
                if (image.name == "tank") {
                    val node = ARNode(context, R.raw.tank)
                    node.setImage(image)
                    arView.scene.addChild(node)
                }
            }
        }
    }

    private fun setupSession() {
        try {
            if (session == null) {
                session = Session(context)
                shoodConfigurateSession = true
            }
            if (shoodConfigurateSession) {
                configSession()
                shoodConfigurateSession = false
                arView.setupSession(session)
            }
            session?.resume()
            arView.resume()
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("!!!", "setupSession() : " + e.stackTrace)
            session = null
            return
        }
    }

    private fun configSession() {
        val config = Config(session)
        if (!buildDatabase(config)) {
            Toast.makeText(context, "Ошибка базы данных", Toast.LENGTH_SHORT).show()
        }
    }

    private fun buildDatabase(config: Config): Boolean {
        val bitmap: Bitmap = loadImage() ?: return false
        val augmentImageDatabase = AugmentedImageDatabase(session)
        augmentImageDatabase.addImage("tank", bitmap)
        config.augmentedImageDatabase = augmentImageDatabase
        return true
    }

    private fun loadImage(): Bitmap? {
        try {
            val input = context.assets.open("qr_code_frame.png")
            return BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("!!!", "loadImage() : " + e.stackTrace)
        }
        return null
    }

    private lateinit var listener: Listener

    override fun setListener(listener: Listener) {
        this.listener = listener
    }

    override fun onResume(activity: Activity) {
        Dexter.withActivity(activity)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object: PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    setupSession()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {

                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    Toast.makeText(context, "Необходим доступ к камере", Toast.LENGTH_SHORT).show()
                }
            }).check()
    }

    override fun onPause(activity: Activity) {
        if (session != null) {
            arView.pause()
            session?.pause()
        }
    }
}