package com.gsu.mk.easyhire.presentation.feature.map.fragment

import com.gsu.mk.easyhire.data.dto.FBListModel
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback

interface IMap: OnMapReadyCallback {
    fun updateMap(model: FBListModel)
}
