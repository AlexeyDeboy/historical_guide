package com.gsu.mk.easyhire.presentation.feature.addplace.fragmentview

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.gsu.mk.easyhire.presentation.feature.addplace.fragmentview.IAddPlaceFragmentView.Listener

class AddPlaceFragmentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): LinearLayout(context, attrs, defStyleAttr), IAddPlaceFragmentView {

    private lateinit var listener: Listener

    override fun setListener(listener: Listener) {
        this.listener = listener
    }
}