package com.gsu.mk.easyhire.presentation.feature.main.fragmentview

import androidx.fragment.app.FragmentManager
import com.gsu.mk.easyhire.data.dto.FBListModel

interface IStartFragmentView {

    fun setListener(listener: Listener)

    fun showProgress()

    fun hideProgress()

    fun populate(fragmentManager: FragmentManager, model: FBListModel?)

    interface Listener {

        fun onScanClick()

        fun onMapClick()

        fun onAddPlaceClick()
    }
}