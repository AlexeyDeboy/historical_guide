package com.gsu.mk.easyhire.presentation.feature.fbobj.fragmentview

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBObject
import com.gsu.mk.easyhire.presentation.feature.fbobj.fragmentview.IDetailsFragmentView.Listener
import com.gsu.mk.easyhire.presentation.feature.util.bind

class DetailsFragmentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): ConstraintLayout(context, attrs, defStyleAttr), IDetailsFragmentView{

    private val titleTextView: TextView by bind(R.id.details_title)
    private val imageView: ImageView by bind(R.id.details_image)
    private val playerButton: ImageButton by bind(R.id.details_player_image_button)
    private val playerSeekBar: SeekBar by bind(R.id.details_player_seek_bar)
    private val textTextView: TextView by bind(R.id.details_text)

    private lateinit var listener: Listener
    private var isOnPause: Boolean = true
    private lateinit var mediaPlayer: MediaPlayer

    override fun populate(model: FBObject) {
        titleTextView.text = model.title
        model.image?.let {
            try {
                Glide.
                    with(context)
                    .load(it)
                    .into(imageView)
            } catch (e: Exception) {
                e.printStackTrace()
                imageView.visibility = View.GONE
            }
        }?:run {
            imageView.visibility = View.GONE
        }
        model.audio?.let {
            initPlayer(it)
        }?:run {
            playerButton.visibility = View.GONE
            playerSeekBar.visibility = View.GONE
        }
        textTextView.text = model.text
        setListeners()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initPlayer(uri: String) {
        try {
            mediaPlayer = MediaPlayer.create(context, Uri.parse(uri))
            playerSeekBar.setOnTouchListener { v, _ ->
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.seekTo((v as SeekBar).progress)
                }
                false
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            playerButton.visibility = View.GONE
            playerSeekBar.visibility = View.GONE
        }
    }

    override fun setListener(listener: Listener) {
        this.listener = listener
    }

    private fun setListeners() {
        playerButton.setOnClickListener {
            if (isOnPause) {
                try {
                    mediaPlayer.start()
                    updateSeekBar()
                } catch (exception: Exception) {
                    mediaPlayer.pause()
                    exception.printStackTrace()
                }
            } else {
                mediaPlayer.pause()
            }
        }
    }

    private fun updateSeekBar() {
        playerSeekBar.progress = mediaPlayer.currentPosition
        if (mediaPlayer.isPlaying) {
            val notification = Runnable {
                updateSeekBar()
            }
            handler.postDelayed(notification, 1000)
        } else {
            mediaPlayer.pause()
            playerSeekBar.progress = 0
        }
    }
}