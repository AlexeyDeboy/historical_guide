package com.gsu.mk.easyhire.presentation.feature.main.fragmentview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.presentation.feature.main.fragmentview.IStartFragmentView.Listener
import com.gsu.mk.easyhire.presentation.feature.map.adapter.RecyclerAdapter
import com.gsu.mk.easyhire.presentation.feature.util.bind

class StartFragmentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): ConstraintLayout(context, attrs, defStyleAttr), IStartFragmentView {

    private val scanButton: FloatingActionButton by bind(R.id.start_scan_button)
    private val addPlaceButton: FloatingActionButton by bind(R.id.start_add_button)
    private val pager: RecyclerView by bind(R.id.start_view_pager)
    private val progress: ProgressBar by bind(R.id.start_progress_bar)

    private lateinit var listener: Listener

    override fun populate(fragmentManager: FragmentManager, model: FBListModel?) {
        val adapter = RecyclerAdapter(arrayListOf(0, 1, 2), model)
        pager.adapter = adapter
    }

    override fun setListener(listener: Listener) {
        this.listener = listener
        setListeners()
    }

    private fun setListeners() {
        scanButton.setOnClickListener { listener.onScanClick() }
        addPlaceButton.setOnClickListener { listener.onAddPlaceClick() }
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }
}