package com.gsu.mk.easyhire.presentation.feature.main.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.firebase.FirebaseHelper
import com.gsu.mk.easyhire.presentation.feature.main.fragment.StartFragment
import com.gsu.mk.easyhire.presentation.feature.map.controller.MapController

class StartActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_fragment_view)
        MapController.context = this
        FirebaseHelper().anonymousAuth()
        supportFragmentManager.beginTransaction()
            .add(R.id.main_fragment, StartFragment.getInstance()).commit()
    }

    companion object {

        fun startActivity(context: Context?) {
            val intent = Intent(context, StartActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context?.startActivity(intent)
        }
    }
}