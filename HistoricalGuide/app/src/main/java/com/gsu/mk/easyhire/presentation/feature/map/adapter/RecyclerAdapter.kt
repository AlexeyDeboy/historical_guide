package com.gsu.mk.easyhire.presentation.feature.map.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.presentation.feature.addplace.activity.AddPlaceActivity
import com.gsu.mk.easyhire.presentation.feature.map.controller.MapController.context
import com.gsu.mk.easyhire.presentation.feature.util.bind

class RecyclerAdapter (private val items: List<Int>, val model: FBListModel?) :
    RecyclerView.Adapter<RecyclerAdapter.TableItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableItemHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.start_card_fragment_view, parent, false)
        return TableItemHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TableItemHolder, position: Int) {
        holder.populate(items[position])
    }

    inner class TableItemHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        private val textView: TextView by itemView.bind(R.id.start_card_fragment_view_text_view)
        private val imageView: ImageView by itemView.bind(R.id.start_card_fragment_view_image_view)

        fun populate(i: Int) {
            when (i) {
                0 -> {
                    imageView.setImageResource(R.drawable.another_gomel)
                    textView.text = "Памятники"
                }
                1 -> {
                    imageView.setImageResource(R.drawable.another_gomel)
                    textView.text = "Другой Гомель"
                }
                else -> {
                    imageView.setImageResource(R.drawable.another_gomel)
                    textView.text = "Скоро"
                }
            }
            itemView.setOnClickListener {
//                MapActivity.startActivity(context, model?.list)
                AddPlaceActivity.startActivity(context)
            }
        }
    }
}