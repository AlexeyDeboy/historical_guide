package com.gsu.mk.easyhire.presentation.feature.scan.fragment

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.PointF
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.presentation.feature.fbobj.activity.DetailsActivity
import kotlinx.android.synthetic.main.fragment_scan.*
import java.util.regex.Pattern

class ScanFragment : Fragment(), OnQRCodeReadListener {

    private val list = arguments?.getParcelable<FBListModel>(EXTRA_MODEL)?.list

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            if (ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    it as Activity,
                    arrayOf(Manifest.permission.CAMERA),
                    1
                )
            }
        }
        (activity as AppCompatActivity).setSupportActionBar(scan_toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initQRReader()
    }

    private fun initQRReader() {
        qr_reader.setOnQRCodeReadListener(this)
        qr_reader.setQRDecodingEnabled(true)
        qr_reader.setAutofocusInterval(2000L)
        qr_reader.setTorchEnabled(true)
        qr_reader.setBackCamera()
    }

    override fun onQRCodeRead(text: String, points: Array<PointF>) {
        qr_reader.stopCamera()
        val patternLat = Pattern.compile("lat=(.*?);")
        val patternLon = Pattern.compile("lon=(.*?)")
        val latMatcher = patternLat.matcher(text)
        val lonMatcher = patternLon.matcher(text)
        var lat = 0.0
        var lon = 0.0
        if (latMatcher.find()) {
            lat = latMatcher.group(1).toDouble()
        }
        if (lonMatcher.find()) {
            lon = lonMatcher.group(1).toDouble()
        }
        val obj = list?.findLast {
            (it.latitude == lat) and (it.longitude == lon)
        }
        if (obj == null) {
            if (!isOnDlg){
                context?.let {
                    isOnDlg = true
                    val dialogBuilder = AlertDialog.Builder(it)
                    dialogBuilder.setMessage(R.string.cant_find)
                        .setCancelable(false)
                        .setPositiveButton(
                            R.string.cancel
                        ) { dialog, _ ->
                            isOnDlg = false
                            qr_reader.startCamera()
                            dialog.cancel()
                        }
                    val alert = dialogBuilder.create()
                    alert.setTitle("")
                    alert.show()
                }
            }
        } else {
            DetailsActivity.startActivity(context, obj)
        }
    }

    override fun onResume() {
        super.onResume()
        qr_reader.startCamera()
    }

    override fun onPause() {
        super.onPause()
        qr_reader.stopCamera()
    }

    companion object {

        var isOnDlg: Boolean = false

        private const val EXTRA_MODEL = "EXTRA_MODEL"

        fun newInstance(model: FBListModel): ScanFragment {
            val fragment = ScanFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_MODEL, model)
            fragment.arguments = args
            return fragment
        }
    }
}
