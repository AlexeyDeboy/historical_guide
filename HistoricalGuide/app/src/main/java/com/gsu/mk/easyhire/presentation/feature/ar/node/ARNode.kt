package com.gsu.mk.easyhire.presentation.feature.ar.node

import android.content.Context
import android.os.Build
import com.google.ar.core.AugmentedImage
import com.google.ar.core.Pose
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import java.util.concurrent.CompletableFuture

class ARNode: AnchorNode {

    private lateinit var image: AugmentedImage

    constructor(context: Context, modelId: Int) {
        if (modelRenderableCompletableFuture == null) {
            modelRenderableCompletableFuture = ModelRenderable.builder()
                .setRegistryId("tank_model")
                .setSource(context, modelId)
                .build()
        }
    }

    fun setImage(image: AugmentedImage) {
        this.image = image
        modelRenderableCompletableFuture?.let {
            if (!it.isDone) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    CompletableFuture.allOf(modelRenderableCompletableFuture)
                        .thenAccept { setImage(image)}.exceptionally { return@exceptionally null }
                }
            }
        }
        anchor = image.createAnchor(image.centerPose)
        val node = Node()
        val pose = Pose.makeTranslation(0.0f, 0.0f, 0.25f)
        node.setParent(this)
        node.localPosition = Vector3(pose.tx(), pose.ty(), pose.tz())
        node.localRotation = Quaternion(pose.qw(), pose.qy(), pose.qz(), pose.qw())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            node.renderable = modelRenderableCompletableFuture?.getNow(null)
        }
    }

    fun getImage() = image

    companion object {

        private var modelRenderableCompletableFuture: CompletableFuture<ModelRenderable>? = null
    }
}