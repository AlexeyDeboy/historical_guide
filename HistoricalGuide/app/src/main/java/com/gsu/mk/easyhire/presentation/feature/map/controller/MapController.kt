package com.gsu.mk.easyhire.presentation.feature.map.controller

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.presentation.feature.fbobj.activity.DetailsActivity
import com.gsu.mk.easyhire.presentation.feature.map.adapter.CustomInfoWindowAdapter
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newLatLngZoom
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import java.util.*
import com.mapbox.mapboxsdk.annotations.MarkerOptions as MarkerOptions1

@SuppressLint("StaticFieldLeak")
object MapController {

    private var DEFAULT_LAT = 52.43
    private var DEFAULT_LNG = 31.00

    lateinit var context: Context

    private var needToFocus = true

    fun fillMap(map: MapView?, model: FBListModel, view: View) {
        val list = model.list
        map?.getMapAsync { mapboxMap ->
            mapboxMap.clear()
            mapboxMap.infoWindowAdapter = CustomInfoWindowAdapter(view)
            if (needToFocus) {
                val centerPosition = getCurrentPosition(context)
                val cameraPosition = com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                    .target(centerPosition)
                    .zoom(15.0)
                    .bearing(0.0)
                    .build()
                mapboxMap.moveCamera(newCameraPosition(cameraPosition))
                needToFocus = false
            }
            val markers = ArrayList<com.mapbox.mapboxsdk.annotations.Marker>()
            for (fbObj in list) {
                val lat = fbObj.latitude
                val lng = fbObj.longitude
                if (lat != 0.0 && lng != 0.0) {
                    val pos = LatLng(lat, lng)
                    val marker: com.mapbox.mapboxsdk.annotations.Marker
                    marker = mapboxMap.addMarker(
                        MarkerOptions1()
                            .position(pos)
                            .title(fbObj.title)
                            .snippet(fbObj.description)
                            .icon(IconFactory.getInstance(context).fromBitmap(getBitmap(R.drawable.ic_pin)))
                    )
                    markers.add(marker)
                }
            }

            model.lat?.let { lat ->
                model.lon?.let { lon ->
                    mapboxMap.animateCamera(newLatLngZoom(LatLng(lat, lon), 15.0))
                    markers.forEach { marker ->
                        if ((marker.position.latitude == model.lat) and (marker.position.longitude == model.lon)) {
                            marker.showInfoWindow(mapboxMap, map)
                            clearSelected(markers)
                            marker.icon = IconFactory.getInstance(context).fromBitmap(getBitmap(R.drawable.ic_pin_active))
                            view.findViewById<Button>(R.id.marker_card_details_button).setOnClickListener {
                                val fbObj = list.findLast { (it.latitude == marker.position.latitude) and (it.longitude == marker.position.longitude) }
                                fbObj?.let {
                                    DetailsActivity.startActivity(context, fbObj)
                                }
                            }
                        }
                    }
                }
            }

            mapboxMap.addOnMapClickListener{
                view.visibility = View.GONE
                clearSelected(markers)
                true
            }
            mapboxMap.setOnMarkerClickListener { marker ->
                marker.showInfoWindow(mapboxMap, map)
                clearSelected(markers)
                marker.icon = IconFactory.getInstance(context).fromBitmap(getBitmap(R.drawable.ic_pin_active))
                view.findViewById<Button>(R.id.marker_card_details_button).setOnClickListener {
                    val fbObj = list.findLast { (it.latitude == marker.position.latitude) and (it.longitude == marker.position.longitude) }
                    fbObj?.let {
                        DetailsActivity.startActivity(context, fbObj)
                    }
                    val imageView = view.findViewById<ImageView>(R.id.marker_card_image_view)
                    try {
                        Glide.
                            with(context)
                            .load(it)
                            .into(imageView)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        imageView.visibility = View.GONE
                    }
                }

                true
            }
        }
    }

    fun focusAtCurrentPos(map: MapView?) {
        map?.getMapAsync { googleMap ->
            val centerPosition = getCurrentPosition(context)
            val cameraPosition = CameraPosition.Builder()
                .target(centerPosition)
                .zoom(15.0)
                .bearing(0.0)
                .build()
            googleMap.animateCamera(newCameraPosition(cameraPosition))
        }
    }

    private fun getBitmap(drawableRes: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableRes)
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth?:0, drawable?.intrinsicHeight?:0, Bitmap.Config.ARGB_8888)
        canvas.setBitmap(bitmap)
        drawable?.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable?.draw(canvas)

        return bitmap
    }

    private fun clearSelected(markers: List<com.mapbox.mapboxsdk.annotations.Marker>) {
        for (marker in markers) {
            marker.icon = IconFactory.getInstance(context).fromBitmap(getBitmap(R.drawable.ic_pin))
        }
    }

    private fun getCurrentPosition(context: Context): LatLng? {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    context as Activity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
            }
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    context as Activity,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    1
                )
            }
            return LatLng(DEFAULT_LAT, DEFAULT_LNG)
        }
        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {

            }

            override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {

            }

            override fun onProviderEnabled(s: String) {

            }

            override fun onProviderDisabled(s: String) {

            }
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListener)
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, locationListener)

        val gps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        val network = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
        var location: Location? = null
        if (gps != null && network != null) {
            location = if (gps.time > network.time) {
                gps
            } else {
                network
            }
        } else if (gps != null) {
            location = gps
        } else if (network != null) {
            location = network
        }

        if (location != null) {
            val lat = location.latitude
            val lng = location.longitude
            return LatLng(lat, lng)
        }
        return LatLng(DEFAULT_LAT, DEFAULT_LNG)
    }
}
