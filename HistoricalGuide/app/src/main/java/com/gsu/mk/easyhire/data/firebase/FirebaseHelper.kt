package com.gsu.mk.easyhire.data.firebase

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBObject
import java.util.*

class FirebaseHelper {

    companion object {

        private const val OBJECTS_NODE = "objects"
    }

    val objectsLiveData: MutableLiveData<List<FBObject>> = MutableLiveData()

    fun loadData(context: Context?) {
        val database = FirebaseDatabase.getInstance()
        val trNode = database.getReference(OBJECTS_NODE)
        trNode.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val list = ArrayList<FBObject>()
                if (dataSnapshot.value != null) {
                    for (child in dataSnapshot.children) {
                        try {
                            list.add(FBObject(
                                child.child("audio").value as String?,
                                child.child("description").value as String,
                                child.child("image").value as String?,
                                child.child("latitude").value as Double,
                                child.child("longitude").value as Double,
                                child.child("text").value as String,
                                child.child("title").value as String
                            ))
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    objectsLiveData.value = list
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                context?.let {
                    val dialogBuilder = AlertDialog.Builder(it)
                    dialogBuilder.setMessage(R.string.check_connection)
                        .setCancelable(false)
                        .setPositiveButton(
                            R.string.cancel
                        ) { dialog, _ ->
                            dialog.cancel()
                        }
                    val alert = dialogBuilder.create()
                    alert.setTitle("")
                    alert.show()
                }
            }
        })
    }

    fun anonymousAuth() {
        val auth = FirebaseAuth.getInstance()
        auth.currentUser
        auth.signInAnonymously()
    }
}
