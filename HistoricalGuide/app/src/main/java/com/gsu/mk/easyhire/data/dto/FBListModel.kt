package com.gsu.mk.easyhire.data.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class FBListModel (
    val list: List<FBObject>,
    var lat: Double? = null,
    var lon: Double? = null
) : Parcelable