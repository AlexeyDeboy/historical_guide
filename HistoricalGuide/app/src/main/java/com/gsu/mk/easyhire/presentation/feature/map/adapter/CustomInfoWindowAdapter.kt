package com.gsu.mk.easyhire.presentation.feature.map.adapter

import android.content.Context
import android.view.View
import android.widget.TextView
import com.gsu.mk.easyhire.R
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.maps.MapboxMap

class CustomInfoWindowAdapter(private val window: View) : MapboxMap.InfoWindowAdapter {

    private val context: Context = window.context

    private fun setWindowText(marker: Marker, view: View) {
        val name = marker.title
        val address = marker.snippet

        val nameView = view.findViewById<TextView>(R.id.marker_card_type_textView)
        val addressView = view.findViewById<TextView>(R.id.marker_card_model_textView)

        window.visibility = View.VISIBLE
        nameView.text = name
        addressView.text = address
    }

    override fun getInfoWindow(marker: Marker): View? {
        setWindowText(marker, window)
        return TextView(context)
    }

//    override fun getInfoContents(marker: Marker): View {
//        setWindowText(marker, window)
//        val view = View(context)
//        view.isActivated = false
//        return TextView(context)
//    }
}
