package com.gsu.mk.easyhire.presentation.feature.util

import android.content.Context
import android.util.AttributeSet
import androidx.viewpager.widget.ViewPager
import com.gsu.mk.easyhire.presentation.feature.main.adapter.StartCardsAdapter

class MeasuredViewPager  @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : ViewPager(context, attrs) {

    var swipeAdapter: StartCardsAdapter? = null

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var height: Int
        val view = swipeAdapter?.currentView
        view?.let { view ->
            view.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED))
            height =
                view.measuredHeight
            val heightMeasureSpecNew = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
            super.onMeasure(widthMeasureSpec, heightMeasureSpecNew)
        } ?: run {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }
}