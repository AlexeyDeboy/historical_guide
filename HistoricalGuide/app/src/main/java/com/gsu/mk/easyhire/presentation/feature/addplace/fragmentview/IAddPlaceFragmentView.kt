package com.gsu.mk.easyhire.presentation.feature.addplace.fragmentview

interface IAddPlaceFragmentView {

    fun setListener(listener: Listener)

    interface Listener {

        fun onBackPressed()
    }
}