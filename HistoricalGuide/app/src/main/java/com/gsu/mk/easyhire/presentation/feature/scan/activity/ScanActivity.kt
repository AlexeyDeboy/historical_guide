package com.gsu.mk.easyhire.presentation.feature.scan.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.data.firebase.FirebaseHelper
import com.gsu.mk.easyhire.presentation.feature.main.activity.StartActivity
import com.gsu.mk.easyhire.presentation.feature.scan.fragment.ScanFragment

class ScanActivity: AppCompatActivity() {

    private val firebaseHelper = FirebaseHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_fragment_view)
        firebaseHelper.objectsLiveData.observe(this, Observer {
            supportFragmentManager.beginTransaction()
                .add(R.id.main_fragment, ScanFragment.newInstance(FBListModel(it))).commit()
        })
        firebaseHelper.loadData(this)
    }

    override fun onBackPressed() {
        StartActivity.startActivity(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return true
    }

    companion object {

        fun startActivity(context: Context?) {
            val intent = Intent(context, ScanActivity::class.java)
            context?.startActivity(intent)
        }
    }
}