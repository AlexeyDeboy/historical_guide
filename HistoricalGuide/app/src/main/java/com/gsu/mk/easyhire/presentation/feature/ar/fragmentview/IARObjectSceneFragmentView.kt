package com.gsu.mk.easyhire.presentation.feature.ar.fragmentview

import android.app.Activity

interface IARObjectSceneFragmentView {

    fun populate(activity: Activity)

    fun setListener(listener: Listener)

    fun onResume(activity: Activity)

    fun onPause(activity: Activity)

    interface Listener {

        fun onBackPressed()
    }
}