package com.gsu.mk.easyhire.presentation.feature.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.presentation.feature.map.activity.MapActivity
import kotlinx.android.synthetic.main.start_card_fragment_view.*

class StartCardFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.start_card_fragment_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (arguments?.getInt(EXTRA_NUMBER)?:0) {
            0 -> {
                start_card_fragment_view_image_view.setImageResource(R.drawable.another_gomel)
                start_card_fragment_view_text_view.text = "Памятники"
                //arguments?.getParcelable<FBListModel?>(EXTRA_MODEL)?.list?.all { it.title != "" } todo открывать с отбором
            }
            1 -> {
                start_card_fragment_view_image_view.setImageResource(R.drawable.another_gomel)
                start_card_fragment_view_text_view.text = "Другой Гомель"
            }
            else -> {
                start_card_fragment_view_image_view.setImageResource(R.drawable.another_gomel)
                start_card_fragment_view_text_view.text = "Скоро"
            }
        }
        start_card_fragment_view_card_view.setOnClickListener {
            MapActivity.startActivity(context, arguments?.getParcelable<FBListModel?>(EXTRA_MODEL)?.list)
        }
    }

    companion object {

        private const val EXTRA_NUMBER = "EXTRA_NUMBER"
        private const val EXTRA_MODEL = "EXTRA_MODEL"

        fun getInstance(number: Int, model: FBListModel?): StartCardFragment {
            val fragment = StartCardFragment()
            val args = Bundle()
            args.putInt(EXTRA_NUMBER, number)
            args.putParcelable(EXTRA_MODEL, model)
            fragment.arguments = args
            return fragment
        }
    }
}