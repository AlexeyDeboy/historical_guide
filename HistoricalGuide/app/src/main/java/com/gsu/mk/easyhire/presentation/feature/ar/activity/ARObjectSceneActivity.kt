package com.gsu.mk.easyhire.presentation.feature.ar.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.presentation.feature.ar.fragment.ARObjectSceneFragment

class ARObjectSceneActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_fragment_view)
        supportFragmentManager.beginTransaction()
            .add(
                R.id.main_fragment, ARObjectSceneFragment.getInstance()).commit()
    }

    companion object {

        fun startActivity(context: Context?) {
            val intent = Intent(context, ARObjectSceneActivity::class.java)
            context?.startActivity(intent)
        }
    }
}