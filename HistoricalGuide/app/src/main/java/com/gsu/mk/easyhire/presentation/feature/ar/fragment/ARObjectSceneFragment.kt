package com.gsu.mk.easyhire.presentation.feature.ar.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.presentation.feature.ar.fragmentview.IARObjectSceneFragmentView
import com.gsu.mk.easyhire.presentation.feature.ar.fragmentview.IARObjectSceneFragmentView.Listener

class ARObjectSceneFragment: Fragment() {

    private lateinit var view: IARObjectSceneFragmentView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.ar_object_scene_fragment_view, container, false)
        view = root as IARObjectSceneFragmentView
        return root
    }

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)
        view.setListener(object : Listener {

            override fun onBackPressed() {
                activity?.onBackPressed()
            }
        })
        activity?.let {
            view.populate(it)
        }
    }

    companion object {

        fun getInstance() = ARObjectSceneFragment()
    }
}