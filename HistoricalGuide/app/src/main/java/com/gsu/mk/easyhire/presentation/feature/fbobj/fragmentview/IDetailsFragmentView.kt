package com.gsu.mk.easyhire.presentation.feature.fbobj.fragmentview

import com.gsu.mk.easyhire.data.dto.FBObject

interface IDetailsFragmentView {

    fun populate(model: FBObject)

    fun setListener(listener: Listener)

    interface Listener {

        fun onBackClick()

        fun onScanTabClick()

        fun onShowModelClick()
    }
}