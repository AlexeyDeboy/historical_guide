package com.gsu.mk.easyhire.presentation.feature.addplace.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.presentation.feature.addplace.fragment.AddPlaceFragment

class AddPlaceActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_fragment_view)
            supportFragmentManager.beginTransaction()
                .add(R.id.main_fragment, AddPlaceFragment.getInstance()).commit()
        }

    companion object {

        fun startActivity(context: Context?) {
            val intent = Intent(context, AddPlaceActivity::class.java)
            context?.startActivity(intent)
        }
    }
}