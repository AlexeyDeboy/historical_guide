package com.gsu.mk.easyhire.presentation.feature.map.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.data.dto.FBObject
import com.gsu.mk.easyhire.data.firebase.FirebaseHelper
import com.gsu.mk.easyhire.presentation.feature.main.activity.StartActivity
import com.gsu.mk.easyhire.presentation.feature.map.controller.MapController
import com.gsu.mk.easyhire.presentation.feature.map.fragment.MapFragment
import java.util.regex.Pattern

class MapActivity: AppCompatActivity() {

    private val firebaseHelper = FirebaseHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_fragment_view)
        MapController.context = this
        val model = intent?.extras?.getParcelable<FBListModel>(EXTRA_LIST)
        model?.let {
            load(it)
        }?:run {
            firebaseHelper.objectsLiveData.observe(this, Observer { load(FBListModel(it)) })
            firebaseHelper.loadData(this)
        }
    }

    private fun load(model: FBListModel) {
        intent.dataString?.let {uri ->
            if (uri.contains("location")) {
                val patternLat = Pattern.compile("lat=(.*?);")
                val patternLon = Pattern.compile("lon=(.*?)")
                val latMatcher = patternLat.matcher(uri)
                val lonMatcher = patternLon.matcher(uri)
                if (latMatcher.find()) {
                    model.lat = latMatcher.group(1).toDouble()
                }
                if (lonMatcher.find()) {
                    model.lon = lonMatcher.group(1).toDouble()
                }
            }
        }
        supportFragmentManager.beginTransaction()
            .add(R.id.main_fragment, MapFragment.newInstance(model)).commit()
    }

    override fun onBackPressed() {
        StartActivity.startActivity(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    companion object {

        private const val EXTRA_LIST = "EXTRA_LIST"

        fun startActivity(context: Context?, list: List<FBObject>?) {
            val intent = Intent(context, MapActivity::class.java)
            list?.let {
                intent.putExtra(EXTRA_LIST, FBListModel(it))
            }
            context?.startActivity(intent)
        }
    }
}