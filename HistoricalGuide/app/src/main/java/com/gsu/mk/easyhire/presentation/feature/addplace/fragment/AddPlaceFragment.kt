package com.gsu.mk.easyhire.presentation.feature.addplace.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gsu.mk.easyhire.R

class AddPlaceFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.add_place_fragment_view, container, false)
    }

    companion object {

        fun getInstance() = AddPlaceFragment()
    }
}