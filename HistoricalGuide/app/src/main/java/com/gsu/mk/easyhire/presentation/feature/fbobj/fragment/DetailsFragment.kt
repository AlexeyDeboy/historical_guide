package com.gsu.mk.easyhire.presentation.feature.fbobj.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBObject
import com.gsu.mk.easyhire.presentation.feature.fbobj.fragmentview.IDetailsFragmentView
import com.gsu.mk.easyhire.presentation.feature.fbobj.fragmentview.IDetailsFragmentView.Listener

class DetailsFragment: Fragment() {

    private lateinit var view: IDetailsFragmentView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.details_fragment_view, container, false)
        view = root as IDetailsFragmentView
        return root
    }

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)
        view.setListener(object: Listener {

            override fun onBackClick() {
                activity?.onBackPressed()
            }

            override fun onScanTabClick() {

            }

            override fun onShowModelClick() {

            }
        })
        arguments?.getParcelable<FBObject?>(EXTRA_MODEL)?.let {
            view.populate(it)
        }
    }

    companion object {

        private const val EXTRA_MODEL = "EXTRA_MODEL"

        fun getInstance(model: FBObject?): DetailsFragment {
            val fragment = DetailsFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_MODEL, model)
            fragment.arguments = args
            return fragment
        }
    }
}