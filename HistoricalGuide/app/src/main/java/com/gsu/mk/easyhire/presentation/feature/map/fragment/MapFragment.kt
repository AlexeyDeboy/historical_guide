package com.gsu.mk.easyhire.presentation.feature.map.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.presentation.feature.ar.activity.ARObjectSceneActivity
import com.gsu.mk.easyhire.presentation.feature.map.controller.MapController
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.fragment_map.view.*

class MapFragment : Fragment(), IMap {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        context?.let {
            Mapbox.getInstance(it, resources.getString(R.string.mapbox_access_token))
        }
        val root = inflater.inflate(R.layout.fragment_map, container, false)
        root.map_fr.onCreate(savedInstanceState)
        root.map_fr.getMapAsync(this)
        return root
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        mapboxMap.setStyle(Style.MAPBOX_STREETS)
        arguments?.getParcelable<FBListModel>(EXTRA_LIST)?.let { model ->
            updateMap(model)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        map_toolbar.setOnMenuItemClickListener {
            activity?.onBackPressed()
            true
        }
        map_focus_button.setOnClickListener {
            MapController.focusAtCurrentPos(
                map_fr
            )
        }
        (activity as AppCompatActivity).setSupportActionBar(map_toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        map_info_card.setOnClickListener {}
        marker_card_show_model_image_view.setOnClickListener {
            ARObjectSceneActivity.startActivity(context)
        }
    }

    override fun updateMap(model: FBListModel) {
        MapController.fillMap(
            map_fr,
            model,
            map_info_card
        )
    }

    override fun onStart() {
        super.onStart()
        map_fr.onStart()
    }

    override fun onStop() {
        super.onStop()
        map_fr.onStop()
    }

    override fun onResume() {
        super.onResume()
        map_fr.onResume()
    }

    override fun onPause() {
        super.onPause()
        map_fr.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map_fr.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        map_fr.onSaveInstanceState(outState)
    }

    companion object {

        private const val EXTRA_LIST = "EXTRA_LIST"

        fun newInstance(list: FBListModel?): MapFragment {
            val fragment = MapFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_LIST, list)
            fragment.arguments = args
            return fragment
        }
    }
}
