package com.gsu.mk.easyhire.presentation.feature.main.adapter

import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.presentation.feature.main.fragment.StartCardFragment

class StartCardsAdapter(val fm: FragmentManager, val model: FBListModel?): FragmentStatePagerAdapter(fm) {

    var currentView: View? = null

    override fun getItem(position: Int): Fragment {
        return StartCardFragment.getInstance(position, model)
    }

    override fun getCount(): Int = 3

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        currentView = (`object` as Fragment).view
        super.setPrimaryItem(container, position, `object`)
    }
}