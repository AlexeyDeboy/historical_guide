package com.gsu.mk.easyhire.presentation.feature.fbobj.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBObject
import com.gsu.mk.easyhire.presentation.feature.fbobj.fragment.DetailsFragment

class DetailsActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_fragment_view)
        supportFragmentManager.beginTransaction()
            .add(R.id.main_fragment, DetailsFragment.getInstance(
                intent.extras?.getParcelable(EXTRA_MODEL)
            )).commit()
    }

    companion object {

        private const val EXTRA_MODEL = "EXTRA_MODEL"

        fun startActivity(context: Context?, model: FBObject?) {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(EXTRA_MODEL, model)
            context?.startActivity(intent)
        }
    }
}