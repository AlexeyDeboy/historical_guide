package com.gsu.mk.easyhire.presentation.feature.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.gsu.mk.easyhire.R
import com.gsu.mk.easyhire.data.dto.FBListModel
import com.gsu.mk.easyhire.data.firebase.FirebaseHelper
import com.gsu.mk.easyhire.presentation.feature.main.fragmentview.IStartFragmentView
import com.gsu.mk.easyhire.presentation.feature.scan.activity.ScanActivity

class StartFragment: Fragment() {

    private lateinit var view: IStartFragmentView

    val firebaseHelper = FirebaseHelper()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.start_fragment_view, container, false)
        view = root as IStartFragmentView
        return root
    }

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)
        firebaseHelper.objectsLiveData.observe(this, Observer {
            view.hideProgress()
            fragmentManager?.let { fragmentManager ->
                view.populate(fragmentManager, FBListModel(it))
            }
        })
        view.showProgress()
        firebaseHelper.loadData(context)
        view.setListener(object: IStartFragmentView.Listener {

            override fun onScanClick() {
                ScanActivity.startActivity(context)
            }

            override fun onMapClick() {

            }

            override fun onAddPlaceClick() {

            }
        })
    }

    companion object {

        fun getInstance(): StartFragment = StartFragment()
    }
}